<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        @include('includes.docs.api.v1.head')
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-3" id="sidebar">
                    <div class="column-content">
                        <div class="search-header">
                            <img src="/assets/docs/api.v1/img/f2m2_logo.svg" class="logo" alt="Logo" />
                            <input id="search" type="text" placeholder="Search">
                        </div>
                        <ul id="navigation">

                            <li><a href="#introduction">Introduction</a></li>

                            

                            <li>
                                <a href="#Zoho">Zoho</a>
                                <ul>
									<li><a href="#Zoho_Sms">Sms</a></li>
</ul>
                            </li>


                        </ul>
                    </div>
                </div>
                <div class="col-9" id="main-content">

                    <div class="column-content">

                        @include('includes.docs.api.v1.introduction')

                        <hr />

                                                

                                                <a href="#" class="waypoint" name="Zoho"></a>
                        <h2>Zoho</h2>
                        <p></p>

                        
                        <a href="#" class="waypoint" name="Zoho_Sms"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>Sms</h3></li>
                            <li>api/v1/sendSms</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite enviar mensajes de texto por medio de una API, recibiendo los parametros necesarios para esto, en caso tal que el usuario no exista, se crea y posteriormente se envia el sms.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/sendSms" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">token</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Token de autenticacion con el api de servicios.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="token">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">phone_number</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">Nombre: Numero de celular al que se le enviara el sms.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="phone_number">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">from_phone_number</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">Nombre: Numero celular del remitente.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="from_phone_number">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">body_text</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Cuerpo del mensaje de texto.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="body_text">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">first_name</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Primer nombre del nuevo usuario para registrar..</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="first_name">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">last_name</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Segundo nombre del nuevo usuario para registrar..</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="last_name">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>


                    </div>
                </div>
            </div>
        </div>


    </body>
</html>
