<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ZohoController extends Controller
{
	/**
    * Este servicio permite enviar mensajes de texto por medio de una API, recibiendo los parametros necesarios para esto, en caso tal que el usuario no exista, se crea y posteriormente se envia el sms.
    * 
    * @param string $token Nombre: Token de autenticacion con el api de servicios.
    * @param integer $phone_number Nombre: Numero de celular al que se le enviara el sms.
    * @param integer $from_phone_number Nombre: Numero celular del remitente.
    * @param string $body_text Nombre: Cuerpo del mensaje de texto.
    * @param string $first_name (Opcional) Nombre: Primer nombre del nuevo usuario para registrar..
    * @param string $last_name (Opcional) Nombre: Segundo nombre del nuevo usuario para registrar..
    *
    * @return Single Response
    */
    public function Sms(Request $request){

    	$conexion_skipio = $this->connectSkipio();

    	if ($conexion_skipio['success'] == false) {
    		$msj = "No se pudo conectar con Skipio, revise las credenciales.";
            return $this->jsonResponse(false, "Operación de envío de sms", $msj,0006,null,204);
    	}

    	if (!empty($request->get('token')) && !empty($request->get('phone_number')) && !empty($request->get('from_phone_number')) && !empty($request->get('body_text'))) {
    		
    		if (env('TOKEN_API') == $request->get('token')) {
    			
    			$contacto = $this->contactExists($request->get('phone_number'),$conexion_skipio);

    			if ($contacto['success']) {
    				//send sms
					if($this->sendSms($contacto['uuid'],$request->get('body_text'),$conexion_skipio)){

						$msj = "Se envió el sms correctamente a su destino.";
            			return $this->jsonResponse(true, "Operación de envío de sms", $msj,0005,null,200);

					}else{
						$msj = "Ocurrio un error en la creación del sms en Skipio, revise e intente nuevamente.";
            				return $this->jsonResponse(false, "Operación de envío de sms", $msj,0004,null,204);
					}

    			}else{
    				//create user
    				$create_user = $this->createUser($request->get('phone_number'),$request->get('first_name'),$request->get('last_name'),$conexion_skipio);
    				if ($create_user['success']) {
    					//send sms
    					if($this->sendSms($create_user['uuid'],$request->get('body_text'),$conexion_skipio)){

    						$msj = "Se creó el usuario y se envió el sms correctamente a su destino.";
            				return $this->jsonResponse(true, "Operación de envío de sms", $msj,0005,null,200);

    					}else{
    						$msj = "Ocurrio un error en la creación del sms en Skipio, revise e intente nuevamente.";
            				return $this->jsonResponse(false, "Operación de envío de sms", $msj,0004,null,204);
    					}
    				}else{
    					$msj = "No se pudo continuar con la operación, debido a que se intento crear el usuario y ocurio un error en el proceso.";
            			return $this->jsonResponse(false, "Operación de envío de sms", $msj,0003,null,204);
    				}
    			}

    		}else{
    			$msj = "El token de la aplicacion no corresponde al actual, solicitelo nuevamente a su administrador de sistema.";
            	return $this->jsonResponse(false, "Operación de envío de sms", $msj,0002,null,401);
    		}

    	}else{
    		$msj = "No se ingresó la información requerida, revise los campos obligatorios e intente de nuevo.";
            return $this->jsonResponse(false, "Operación de envío de sms", $msj,0001,null,400);
    	}

    }

    public function connectSkipio(){

    	$url = env('AUTH_SKIPIO');
		$client = new \GuzzleHttp\Client();
	    $response = $client->request('POST', $url, [
	        'form_params' => [
	            'email' => env('USER_SKIPIO'),
	            'password' => env('PASSWORD_SKIPIO')
	        ]
	    ]);

	   	$token = json_decode($response->getBody());

	   	if (!empty($token)) {
	   		$respuesta = array("success" => true, "token" => $token->token);
	   		return $respuesta;
	   	}else{
	   		$respuesta = array("success" => false, "token" => null);
	   		return $respuesta;
	   	}
    }

    public function contactExists($phone_number,$conexion_skipio){

    	$url = env('SEARCH_CONTACT')."?query=".$phone_number;
		$client = new \GuzzleHttp\Client();
	    $request = $client->get($url,['headers' => [
	        'Content-Type'     => 'application/json',
	        'VND.SKIPIO.TOKEN'      => $conexion_skipio['token']
	    ]]);
	    $contacto = json_decode($request->getBody());

	    if (isset($contacto->data) && sizeof($contacto->data) > 0) {
	    	$respuesta = array("success" => true, "uuid" => $contacto->data[0]->id);
		   	return $respuesta;
	    }else{
	    	$respuesta = array("success" => false, "uuid" => null);
		   	return $respuesta;
	    }
    }

    public function sendSms($uuid,$message_sms,$conexion_skipio){

    	if (!empty($uuid) && !empty($message_sms)) {
    		
	    	$uuid_ = "contact-".$uuid;

		    $sms = [
					"recipients" => [$uuid_],
					"message" => ["body" => $message_sms]
				];

	    	$url = env('SEND_SMS').$conexion_skipio['token'];
			$client = new \GuzzleHttp\Client();
		    $response = $client->request('POST', $url, ['json' => $sms]);

		   	$sms_send = json_decode($response->getBody());
		   	$sms_send_code = $response->getStatusCode();

		   	if (env('APP_DEBUG_SKIPIO')) {
		   		\Log::info($sms_send);
		   	}

		   	if ($sms_send_code == 200 || $sms_send_code == 201) {
		   		return true;
		   	}else{
		   		return false;
		   	}
		}else{
			return false;
		}
    }

    public function createUser($phone_number,$first_name,$last_name,$conexion_skipio){

    	if (!empty($first_name) && !empty($last_name) && !empty($phone_number)) {
    		
	    	$new_contact['contact'] = [
	    							"first_name" => $first_name,
	    							"last_name" => $last_name,
	    							"email" => "test@test.com",
	    							"phone_mobile" => $phone_number
	    								];

	    	$url = env('CREATE_CONTACT_SKIPIO').$conexion_skipio['token'];
			$client = new \GuzzleHttp\Client();
		    $response = $client->request('POST', $url, ['json' => $new_contact]);

		   	$new_contact_created = json_decode($response->getBody());

		   	if (isset($new_contact_created->data) && !empty($new_contact_created->data)) {
		   		$respuesta = array("success" => true, "uuid" => $new_contact_created->data->id);
		   		return $respuesta;
		   	}else{
		   		$respuesta = array("success" => false, "uuid" => null);
		   		return $respuesta;
		   	}
		}else{
			$respuesta = array("success" => false, "uuid" => null);
		   	return $respuesta;
		}
    }

    public function transactionsUncategorized(){

    	$auth = "Basic ".env('URL_CARDCONNECT_TOKEN');
    	$url = env('URL_CARDCONNECT')."funding?merchid=".env('MERCHID_CARDCONNECT');
		$client = new \GuzzleHttp\Client(['headers' => ['Authorization' => $auth, 'Content-Type' => 'application/json']]);
	    $response = $client->request('GET', $url);

	   	$new_contact_created = json_decode($response->getBody());

	   	if (sizeof($$new_contact_created->txns) > 0) {

	   		$transactions = [];

	   		foreach ($$new_contact_created->txns as $key => $transaction) {
	   			
		   		if(strpos($transaction->amount, "-") === false) {
		   			$transaction_type = "credit";
		   		}else{
					$transaction_type = "debit";
		   		}

		   		if ($transaction->cardtype == "Credit") {
		   			$cardtype = "credit";
		   		}else{
		   			$cardtype = "debit";
		   		}

		   		$description = "cardbrand: ".$transaction->cardbrand.",";
		   		$description .= " cardnumber: ".$transaction->cardnumber.",";
		   		$description .= " cardtype: ".$cardtype.",";
		   		$description .= " fundingid: ".$transaction->fundingid.",";
		   		$description .= " type: ".$transaction->type.",";
		   		$description .= " batchid: ".$transaction->batchid.",";
		   		$description .= " currency: ".$transaction->currency;

		   		$transactions[] = array(
	                    	'date' => $transaction->date,
	                    	'debit_or_credit' => $transaction_type,
	                    	'amount' => $transaction->amount*1,
	                    	'description' => $description,
	                    	'reference_number' => $transaction->retref
	                    	);
	   		}


	   		$send_tmp = array(
	   				'account_id' => "1198953000000119013",
                    'transactions' => $transactions
                );


	   		if (sizeof($send_tmp) > 0) {
	   			
	   			$send = json_encode($send_tmp);

	            $url_books = env('CREATE_BANK_ACCOUNTS_TRANSACTIONS_ZOHO_BOOKS')."?organization_id=".env('ORGANIZATION_ID_ZOHO_BOOKS');
		        $client_books = new \GuzzleHttp\Client();
		        $request_books = $client_books->request('POST', $url_books, ['form_params' => ['JSONString' => $send],'headers' => [
		                                        'Content-Type'     => 'application/x-www-form-urlencoded;charset=UTF-8',
		                                        'Authorization'      => env('ZOHO_BOOKS_TOKEN')
		                                    ]]);

		        $transactions_books = json_decode($request_books->getBody());

		        return $this->jsonResponse(true,'Operacion de transacciones','Transacciones procesadas exitosamente',0,$send,200);
		    }

		    return $this->jsonResponse(false,'Operacion de transacciones','Ocurrio un error en el procesamiento de transacciones',0,null,200);

	   	}
    }

    public function test2(){

 /*
    			//Traer todas las transacciones
	            $url_books = "https://books.zoho.com/api/v3/banktransactions?organization_id=".env('ORGANIZATION_ID_ZOHO_BOOKS');
		        $client_books = new \GuzzleHttp\Client();
		        $request_books = $client_books->request("GET",$url_books,['headers' => [
		                                        'Content-Type'     => 'application/x-www-form-urlencoded;charset=UTF-8',
		                                        'Authorization'      => env('ZOHO_BOOKS_TOKEN')
		                                    ]]);

		        $transactions_books = json_decode($request_books->getBody());
*/
/*
		        
    			//Traer UNA las transacciones
	            $url_books = "https://books.zoho.com/api/v3/banktransactions/1198953000004690015?organization_id=".env('ORGANIZATION_ID_ZOHO_BOOKS');
		        $client_books = new \GuzzleHttp\Client();
		        $request_books = $client_books->request("GET",$url_books,['headers' => [
		                                        'Content-Type'     => 'application/x-www-form-urlencoded;charset=UTF-8',
		                                        'Authorization'      => env('ZOHO_BOOKS_TOKEN')
		                                    ]]);

		        $transactions_books = json_decode($request_books->getBody());

		        dd($transactions_books);
*/
/*
		        //Traer cuentas de banco
	            $url_books = "https://books.zoho.com/api/v3/bankaccounts?organization_id=".env('ORGANIZATION_ID_ZOHO_BOOKS');
		        $client_books = new \GuzzleHttp\Client();
		        $request_books = $client_books->request("GET",$url_books,['headers' => [
		                                        'Content-Type'     => 'application/x-www-form-urlencoded;charset=UTF-8',
		                                        'Authorization'      => env('ZOHO_BOOKS_TOKEN')
		                                    ]]);

		        $transactions_books = json_decode($request_books->getBody());

		        dd($transactions_books);
		        */
/*
		        //Traer ULTIMA transaccion no categorizada en determinado banco
	            $url_books = "https://books.zoho.com/api/v3/bankaccounts/1198953000000119013/statement/lastimported?organization_id=".env('ORGANIZATION_ID_ZOHO_BOOKS');
		        $client_books = new \GuzzleHttp\Client();
		        $request_books = $client_books->request("GET",$url_books,['headers' => [
		                                        'Content-Type'     => 'application/x-www-form-urlencoded;charset=UTF-8',
		                                        'Authorization'      => env('ZOHO_BOOKS_TOKEN')
		                                    ]]);

		        $transactions_books = json_decode($request_books->getBody());

		        dd($transactions_books);
		        */
/*
		        //Borrar ULTIMA transaccion no categorizada en determinado banco
	            $url_books = "https://books.zoho.com/api/v3/bankaccounts/1198953000000119013/statement/1198953000004758400?organization_id=".env('ORGANIZATION_ID_ZOHO_BOOKS');
		        $client_books = new \GuzzleHttp\Client();
		        $request_books = $client_books->request("DELETE",$url_books,['headers' => [
		                                        'Content-Type'     => 'application/x-www-form-urlencoded;charset=UTF-8',
		                                        'Authorization'      => env('ZOHO_BOOKS_TOKEN')
		                                    ]]);

		        $transactions_books = json_decode($request_books->getBody());

		        dd($transactions_books);
	   	*/
    }

}
